#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	sound.load("x.mp3");
	sound.play();
	sound.setLoop(true);


	gui.setup();
	gui.add(volume.set("volume", 0.5, 0.0, 1.0));
	gui.add(decay.set("decay", 0.5, 0.0, 1.0));
	gui.add(rotSpeed.set("rotation speed",0 ,0 , 3));
	gui.add(scale.set("scale",500 ,0 , 1000));
	gui.add(scaleX.set("scale X",1 ,0 , 10));
	gui.add(scaleY.set("scale Y",1 ,0 , 10));

	fft = new float[8192];
	for (int i = 0; i < 8192;  i++) {
		fft[i] = 0;
	}

	bands = 1024;
	theta= 0; rotSpeed = 0;

}

//--------------------------------------------------------------
void ofApp::update(){
	ofSoundUpdate();

	sound.setVolume(volume);

	soundSpectrum = ofSoundGetSpectrum(bands);

	for (int i = 0; i < bands; i++) {
		fft[i] *= decay;
		if (fft[i] < soundSpectrum[i]) {
			fft[i] = soundSpectrum[i];
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	//for (int i = 0; i < bands/16 ; i++) {
	//	ofDrawRectangle(ofGetWidth()/2, 128, fft[i] * 100, 128 );
	//	ofDrawCircle(ofGetWidth()/2, ofGetHeight()/2, fft[i] * 100);
	//}

	//float width = (float)(ofGetWidth()) / (bands/2);
	//for (int i = 0;i < bands/2; i++){
	//	// (we use negative height here, because we want to flip them
	//	// because the top corner is 0,0)
	//	ofDrawRectangle(i*width,ofGetHeight(),width,-(fft[i] * i * scale  * ofGetHeight()));
	//}
	gui.draw();

	//circular stuff
	//
	//
	float width = (float)(ofGetWidth()) / (bands/2);
	for (int i = 0;i < bands/2; i++){
	      //float x = scale * cos(theta - fft[i] * 50) ;
	      //float y = scale * sin(theta + fft[i] * 50);
	      float freq_activation = fft[i] * scale;
	      float x = (freq_activation * i ) * cos(theta) ;
	      float y = (freq_activation * i ) * sin(theta);
	      float x_shifted = x + i * width;
	      float y_shifted = y + ofGetHeight()/2;
	      // Drawing in a line
		//ofDrawLine(i * width, ofGetHeight()/2, x_shifted, y_shifted);
		//float circleX =   cos((2 * (1 - rotSpeed)) * (3.2 * (bands/2) * 1.0/i)) * ofGetWidth()/2 + ofGetWidth()/2;
		//float circleY =   sin((3 * (1 + rotSpeed)) * (3.2 * (bands/2) * 1.0/i)) * ofGetHeight()/2 + ofGetHeight()/2;
		float circleX =   cos((scaleX) * (3.2 * (bands/2) * 1.0/i)) * ofGetWidth()/2 + ofGetWidth()/2;
		float circleY =   sin((scaleY) * (3.2 * (bands/2) * 1.0/i)) * ofGetHeight()/2 + ofGetHeight()/2;
		ofDrawLine(circleX, circleY, circleX +  x, circleY + y);
	}



	theta += rotSpeed;
	rotSpeed += 0.001;
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
